// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LabWeek02GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class LABWEEK02_API ALabWeek02GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
