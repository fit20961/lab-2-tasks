// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LabWeek02/LabWeek02GameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLabWeek02GameModeBase() {}
// Cross Module References
	LABWEEK02_API UClass* Z_Construct_UClass_ALabWeek02GameModeBase_NoRegister();
	LABWEEK02_API UClass* Z_Construct_UClass_ALabWeek02GameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_LabWeek02();
// End Cross Module References
	void ALabWeek02GameModeBase::StaticRegisterNativesALabWeek02GameModeBase()
	{
	}
	UClass* Z_Construct_UClass_ALabWeek02GameModeBase_NoRegister()
	{
		return ALabWeek02GameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_ALabWeek02GameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ALabWeek02GameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_LabWeek02,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALabWeek02GameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "LabWeek02GameModeBase.h" },
		{ "ModuleRelativePath", "LabWeek02GameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ALabWeek02GameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ALabWeek02GameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ALabWeek02GameModeBase_Statics::ClassParams = {
		&ALabWeek02GameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ALabWeek02GameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ALabWeek02GameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ALabWeek02GameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ALabWeek02GameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ALabWeek02GameModeBase, 2621036298);
	template<> LABWEEK02_API UClass* StaticClass<ALabWeek02GameModeBase>()
	{
		return ALabWeek02GameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ALabWeek02GameModeBase(Z_Construct_UClass_ALabWeek02GameModeBase, &ALabWeek02GameModeBase::StaticClass, TEXT("/Script/LabWeek02"), TEXT("ALabWeek02GameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ALabWeek02GameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
