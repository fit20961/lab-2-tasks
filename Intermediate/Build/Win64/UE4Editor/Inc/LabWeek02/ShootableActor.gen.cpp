// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LabWeek02/ShootableActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeShootableActor() {}
// Cross Module References
	LABWEEK02_API UClass* Z_Construct_UClass_AShootableActor_NoRegister();
	LABWEEK02_API UClass* Z_Construct_UClass_AShootableActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_LabWeek02();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void AShootableActor::StaticRegisterNativesAShootableActor()
	{
	}
	UClass* Z_Construct_UClass_AShootableActor_NoRegister()
	{
		return AShootableActor::StaticClass();
	}
	struct Z_Construct_UClass_AShootableActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_mVisibleComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_mVisibleComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AShootableActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_LabWeek02,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShootableActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ShootableActor.h" },
		{ "ModuleRelativePath", "ShootableActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AShootableActor_Statics::NewProp_mVisibleComponent_MetaData[] = {
		{ "Category", "ShootableActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "ShootableActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AShootableActor_Statics::NewProp_mVisibleComponent = { "mVisibleComponent", nullptr, (EPropertyFlags)0x0010000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AShootableActor, mVisibleComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AShootableActor_Statics::NewProp_mVisibleComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AShootableActor_Statics::NewProp_mVisibleComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AShootableActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AShootableActor_Statics::NewProp_mVisibleComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AShootableActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AShootableActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AShootableActor_Statics::ClassParams = {
		&AShootableActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AShootableActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AShootableActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AShootableActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AShootableActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AShootableActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AShootableActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AShootableActor, 2077618892);
	template<> LABWEEK02_API UClass* StaticClass<AShootableActor>()
	{
		return AShootableActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AShootableActor(Z_Construct_UClass_AShootableActor, &AShootableActor::StaticClass, TEXT("/Script/LabWeek02"), TEXT("AShootableActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AShootableActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
