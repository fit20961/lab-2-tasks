// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LABWEEK02_CharacterPawn_generated_h
#error "CharacterPawn.generated.h already included, missing '#pragma once' in CharacterPawn.h"
#endif
#define LABWEEK02_CharacterPawn_generated_h

#define LabWeek02_Source_LabWeek02_CharacterPawn_h_17_SPARSE_DATA
#define LabWeek02_Source_LabWeek02_CharacterPawn_h_17_RPC_WRAPPERS
#define LabWeek02_Source_LabWeek02_CharacterPawn_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define LabWeek02_Source_LabWeek02_CharacterPawn_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACharacterPawn(); \
	friend struct Z_Construct_UClass_ACharacterPawn_Statics; \
public: \
	DECLARE_CLASS(ACharacterPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LabWeek02"), NO_API) \
	DECLARE_SERIALIZER(ACharacterPawn)


#define LabWeek02_Source_LabWeek02_CharacterPawn_h_17_INCLASS \
private: \
	static void StaticRegisterNativesACharacterPawn(); \
	friend struct Z_Construct_UClass_ACharacterPawn_Statics; \
public: \
	DECLARE_CLASS(ACharacterPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LabWeek02"), NO_API) \
	DECLARE_SERIALIZER(ACharacterPawn)


#define LabWeek02_Source_LabWeek02_CharacterPawn_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACharacterPawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACharacterPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACharacterPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACharacterPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACharacterPawn(ACharacterPawn&&); \
	NO_API ACharacterPawn(const ACharacterPawn&); \
public:


#define LabWeek02_Source_LabWeek02_CharacterPawn_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACharacterPawn(ACharacterPawn&&); \
	NO_API ACharacterPawn(const ACharacterPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACharacterPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACharacterPawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACharacterPawn)


#define LabWeek02_Source_LabWeek02_CharacterPawn_h_17_PRIVATE_PROPERTY_OFFSET
#define LabWeek02_Source_LabWeek02_CharacterPawn_h_14_PROLOG
#define LabWeek02_Source_LabWeek02_CharacterPawn_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	LabWeek02_Source_LabWeek02_CharacterPawn_h_17_PRIVATE_PROPERTY_OFFSET \
	LabWeek02_Source_LabWeek02_CharacterPawn_h_17_SPARSE_DATA \
	LabWeek02_Source_LabWeek02_CharacterPawn_h_17_RPC_WRAPPERS \
	LabWeek02_Source_LabWeek02_CharacterPawn_h_17_INCLASS \
	LabWeek02_Source_LabWeek02_CharacterPawn_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define LabWeek02_Source_LabWeek02_CharacterPawn_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	LabWeek02_Source_LabWeek02_CharacterPawn_h_17_PRIVATE_PROPERTY_OFFSET \
	LabWeek02_Source_LabWeek02_CharacterPawn_h_17_SPARSE_DATA \
	LabWeek02_Source_LabWeek02_CharacterPawn_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	LabWeek02_Source_LabWeek02_CharacterPawn_h_17_INCLASS_NO_PURE_DECLS \
	LabWeek02_Source_LabWeek02_CharacterPawn_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LABWEEK02_API UClass* StaticClass<class ACharacterPawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID LabWeek02_Source_LabWeek02_CharacterPawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
