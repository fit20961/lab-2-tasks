// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LABWEEK02_LabWeek02GameModeBase_generated_h
#error "LabWeek02GameModeBase.generated.h already included, missing '#pragma once' in LabWeek02GameModeBase.h"
#endif
#define LABWEEK02_LabWeek02GameModeBase_generated_h

#define LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_SPARSE_DATA
#define LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_RPC_WRAPPERS
#define LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALabWeek02GameModeBase(); \
	friend struct Z_Construct_UClass_ALabWeek02GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ALabWeek02GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/LabWeek02"), NO_API) \
	DECLARE_SERIALIZER(ALabWeek02GameModeBase)


#define LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesALabWeek02GameModeBase(); \
	friend struct Z_Construct_UClass_ALabWeek02GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ALabWeek02GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/LabWeek02"), NO_API) \
	DECLARE_SERIALIZER(ALabWeek02GameModeBase)


#define LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALabWeek02GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALabWeek02GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALabWeek02GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALabWeek02GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALabWeek02GameModeBase(ALabWeek02GameModeBase&&); \
	NO_API ALabWeek02GameModeBase(const ALabWeek02GameModeBase&); \
public:


#define LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALabWeek02GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALabWeek02GameModeBase(ALabWeek02GameModeBase&&); \
	NO_API ALabWeek02GameModeBase(const ALabWeek02GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALabWeek02GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALabWeek02GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALabWeek02GameModeBase)


#define LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_12_PROLOG
#define LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_SPARSE_DATA \
	LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_RPC_WRAPPERS \
	LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_INCLASS \
	LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_SPARSE_DATA \
	LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LABWEEK02_API UClass* StaticClass<class ALabWeek02GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID LabWeek02_Source_LabWeek02_LabWeek02GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
